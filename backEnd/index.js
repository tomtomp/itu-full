const express = require('express');
const httpServer = require('http').Server;
const ioSock = require('socket.io');

const app = express();
const http = httpServer(app);
const io = ioSock(http);

app.use(express.static('resources'));

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/resources/index.html');
});

io.on('connection', (socket) => {
    console.log('User connected!');
    socket.on('command', (msg) => {
        if ('text' in msg) {
            var text = msg['text'];

            if (!isNumeric(text)) {
                console.log('Unknown command', text);
            } else {
                sendNumber(parseInt(text));
            }
        }
    });
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});

function isNumeric(str) {
    return !isNaN(parseInt(str)) && isFinite(str);
}

function sendNumber(num) {
    console.log('Sending', num, 'to connected devices.');
    io.emit('play', num);
}

function end() {
    console.log('Server is quitting!');
    process.exit();
}
